## 功能

该脚本配合obsidian的插件QuickAdd的Macro功能, 

来实现把当前笔记中的引用,

比如

```
![01](01.md)
```

转换为01.md的实际的文本内容,

新生成的笔记与要转换的笔记在同一文件夹下, 后缀为时分秒.md

[obsidian运行JS脚本教程](https://www.bilibili.com/video/BV1Ja411q7SB?p=15)



## 思路

提取引用块, 替换引用块原始文本



## 注意事项

我的图片路径用的是markdown的标准格式, 没有使用Wiki链接

```
![aaa](01Sources/04测试/9.png)
```

且, 内部链接类型是: 

```
插入基于当前笔记的相对路径
```

[obsidian图片附件改为markdown格式](https://www.bilibili.com/video/BV1Ja411q7SB?p=8)



## 微信公众号 牙叔教程

## QQ群 747748653